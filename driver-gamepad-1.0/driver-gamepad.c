#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/ioport.h>
#include <linux/interrupt.h>
#include <linux/io.h>
#include <linux/slab.h>
#include <asm/io.h>

#include <linux/fs.h>
#include <linux/sched.h>
#include <linux/errno.h>
#include <asm/current.h>
#include <asm/segment.h>
#include <asm/uaccess.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <asm/siginfo.h>
#include <linux/signal.h>

#define GPIO_PA_BASE 		0x40006000
#define GPIO_PC_BASE 		0x40006048
#define GPIO_PC_MODEL    	(uint32_t*)(GPIO_PC_BASE + 0x04)
#define GPIO_PC_DOUT     	(uint32_t*)(GPIO_PC_BASE + 0x0c)
#define GPIO_EXTIPSELL 		(uint32_t*)(GPIO_PA_BASE + 0x100)
#define GPIO_EXTIPSELH 		(uint32_t*)(GPIO_PA_BASE + 0x104)
#define GPIO_EXTIRISE  		(uint32_t*)(GPIO_PA_BASE + 0x108)
#define GPIO_EXTIFALL  		(uint32_t*)(GPIO_PA_BASE + 0x10c)
#define GPIO_IEN       		(uint32_t*)(GPIO_PA_BASE + 0x110)
#define GPIO_IF       		(uint32_t*)(GPIO_PA_BASE + 0x114)
#define GPIO_IFC       		(uint32_t*)(GPIO_PA_BASE + 0x11c)
#define GPIO_PC_DIN      	(uint32_t*)(GPIO_PC_BASE + 0x1c)
#define ISER0 				(uint32_t*)(0xe000e100)

// Interrupt request numbers
#define GPIO_EVEN_IRQ_NUM 17
#define GPIO_ODD_IRQ_NUM 18

int fops_open		(struct inode *inode,struct file *filep);
int fops_release	(struct inode *inode,struct file *filep);
ssize_t fops_read	(struct file *filep,char *buff,size_t count,loff_t *offp);
ssize_t fops_write	(struct file *filep,const char *buff,size_t count,loff_t *offp);
irqreturn_t GPIO_interrupt_handler(int irq, void *dev_id, struct pt_regs *regs);
static int fops_fasync		(int fd, struct file *filp, int mode);

struct cdev gamepad_cdev;
struct class *c1;
dev_t devno;
struct fasync_struct *async_queue;
struct file_operations fops={
	.owner = THIS_MODULE,
	.read = fops_read,
	.write = fops_write,
	.open = fops_open,
	.release = fops_release,
	.fasync = fops_fasync,
};

int fops_open(struct inode *inode,struct file *filep)
{
	return 0;
}

int fops_release(struct inode *inode,struct file *filep)
{
	fops_fasync(-1, filep, 0);
	return 0;
}
ssize_t fops_read(struct file *filep,char *buff,size_t count,loff_t *offp )
{
	uint32_t data = ioread32(GPIO_PC_DIN);
	copy_to_user(buff, &data, 1);
	return 1;
}
ssize_t fops_write(struct file *filep,const char *buff,size_t count,loff_t *offp )
{
	return 0;
}
static int fops_fasync(int fd, struct file *filp, int mode){
	return fasync_helper(fd, filp, mode, &async_queue);
}

// Interrupt handler
irqreturn_t GPIO_interrupt_handler(int irq, void *dev_id, struct pt_regs *regs){
	if(async_queue){
		kill_fasync(&async_queue, SIGIO, POLL_IN);
	}
    iowrite32(0xFF, GPIO_IFC); // Clear interrupt flag
	return IRQ_HANDLED;
}

static int __init init(void)
{
	printk("Running driver init function\n");

	// Allocate char device number range
	if (alloc_chrdev_region(&devno, 0, 1, "gamepad") < 0){
		printk("Cannot allocate char device region\n");
	}

	cdev_init(&gamepad_cdev, &fops);
	gamepad_cdev.owner = THIS_MODULE;

	// Add char device to system
	int ret;
	if ((ret = cdev_add(&gamepad_cdev, devno, 1)) < 0) {
    	pr_err("Couldn't add device to system: %d", ret, "\n");
	}

	// Create class structure pointer for device_create
	c1 = class_create(THIS_MODULE, "gamepaddriver");

	// Create device and register with system
	if (device_create(c1, NULL, devno, NULL, "gamepaddriver") == NULL){
		printk("Could not create device\n");
	}

	// register char device
	if(register_chrdev(devno, "gamepad_dev", &fops)){
		printk("Failed to register device\n");
		return 1;
	}

	request_mem_region(0x40006048, 0x11c, "driver-gamepad");

	// Enable buttons and interrupt
	iowrite32(0x33333333, GPIO_PC_MODEL);	//Set buttons as input
	iowrite32(0xFF, GPIO_PC_DOUT);			//Enable internal pullup

	request_irq(GPIO_EVEN_IRQ_NUM, (irq_handler_t)GPIO_interrupt_handler, 0, "gamepad", &gamepad_cdev);
    request_irq(GPIO_ODD_IRQ_NUM, (irq_handler_t)GPIO_interrupt_handler, 0, "gamepad", &gamepad_cdev);

	iowrite32(0x22222222, GPIO_EXTIPSELL);	//Enable ctrl of pins 0-7
	iowrite32(0xFF, GPIO_EXTIFALL);			//Interrupt on falling edge
	iowrite32(0xFF, GPIO_IEN);				//Enable interrupts

	printk("Reached end of init function\n");
	return 0;
}

static void __exit cleanup(void)
{
	printk("Short life for a small module...\n");
	//unregister_chrdev(400, "gamepad_dev");

    //Free memory region
	release_mem_region(0x40006048, 0x11c);

	//Remove IRQ
	free_irq(GPIO_EVEN_IRQ_NUM, &gamepad_cdev);
	free_irq(GPIO_ODD_IRQ_NUM, &gamepad_cdev);

	//Remove char dev
	cdev_del(&gamepad_cdev);
}

module_init(init);
module_exit(cleanup);

MODULE_DESCRIPTION("Driver for gamepad supplied in course 'tdt4258' at NTNU");
MODULE_LICENSE("GPL");
