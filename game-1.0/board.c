#include <stdio.h>
#include <stdlib.h>
#include "board.h"
#include "screen.c"


//This function crates (and returns) a 2 dimentional array of (board_)width and (board_)hight
char** board_create(int board_width, int board_hight){
    
    //Alocate a 1dim array
    char **board = (char **)malloc(board_hight * sizeof(char *));
    
    //Make the array a 2dim array
    int row;
    int col;
    for(row = 0; row < board_hight; row++){
        board[row] = (char *)malloc(board_width * sizeof(char));
    }
    
    //Empty the board
    for (row = 0; row < board_hight; row++){
        for (col = 0; col < board_width; col++){
            board[row][col] = ' ';
        }
    }
    
    return board;
}

//This function updates the display with the data that has been witten to the "\dev\fb0" file
void board_update_display(){
    screen_update();
}

//This function fills the board with aliens and a player
void board_init(char** board, int board_width, int board_hight){
    
    //Insert aliens
    int row;
    int col;
    for(row = 0; row < 16; row++){
        if (row%3 != 0){
            for(col = 0; col < board_width - 9 ; col++){ //board_width
                if (col%3 != 0){
                    board[row][col] = 'A';
                }
            }
        }
    }
    
    //Insert player
    board[38][20] = 'P';
    board[38][21] = 'P';
    board[38][22] = 'P';
    board[39][20] = 'P';
    board[39][21] = 'P';
    board[39][22] = 'P';
    
    //Draw the board on the display
    screen_black();
    screen_update();
}


//This function prints the board in the terminal window
void board_print(char** board, int board_width, int board_hight){
    printf("board_print: printing board...\n");
    int row;
    int col;
    for (row = 0; row < board_hight; row++){
        for (col = 0; col < board_width; col++){
            printf("%c", board[row][col]);
        }
        printf("\n");
    }
    printf("board_print: Board has been printed...\n");
}


//This function find the alien whom has the larges pos/col number,
//and return the col number, or -1 if no alien is found.
int board_find_most_right_alien(char** board, int board_width, int board_hight){
    int row;
    int col;
    for (col = board_width -1; col > 0; col--) {
        for (row = 0; row < board_hight-1; row++){
            if (board[row][col] == 'A'){
                return col;
            }
        }
    }
    return -1;
}

//This function find the alien whom has the smallest pos/col number,
//and return the col number, or -1 if no alien is found.
int board_find_most_left_alien(char** board, int board_width, int board_hight){
    int row;
    int col;
    for (col = 0; col < board_width; col++) {
        for (row = 0; row < board_hight; row++){
            if (board[row][col] == 'A'){
                return col;
            }
        }
    }
    return -1;
}

//This function delete/frees the boards alocated memmory.
void board_delete(char** board,int board_width, int board_hight){
    
    //Free the alocated memmory (Delete the board)
    int row;
    for(row = 0; row < board_hight; row++){
        free(board[row]);
    }
    free(board);
    
}


//This function moves all the aliens at the board one position left, rigth, or down, depending on the aliens current direction and positions.
void board_alien_move(char** board, int board_width, int board_hight, int* alien_direction){
    
    //If aliens are moving right
    if (*alien_direction == 1){
        int alien_pos = board_find_most_right_alien(board, board_width, board_hight);
        if (alien_pos+2 < board_width){
            //Aliens move right
            board_alien_move_right(board, board_width, board_hight);
        }
        else if (alien_pos+2 >= board_width) {
            //Alien move down
            board_alien_move_down(board, board_width, board_hight);
            *alien_direction = -1;
        }
    }
    
    //If aliens are moving left
    if (*alien_direction == -1){
        int alien_pos = board_find_most_left_alien(board, board_width, board_hight);
        if (alien_pos >= 1){
            //Aliens move left
            board_alien_move_left(board, board_width, board_hight);
        }
        else if (alien_pos < 1) {
            //Alien move down
            board_alien_move_down(board, board_width, board_hight);
            *alien_direction = 1;
        }
    }
}


//Moves all the aliens in the board one position to the right in the board and on the screen.
void board_alien_move_right(char** board, int board_width, int board_hight){
    
    int row;
    int col;
    for (row = board_hight -1; row >= 0; row--) {
        for (col = board_width -1; col >= 0; col--) {
            if (board[row][col] == 'A') {
                
                //If there is a bullet/shot in the next frame
                if (board[row][col+1] == 'S'){
                    board_alien_hit(board, board_width, board_hight, row, col);
                }
                else{
                    board[row][col] = ' ';
                    screen_block_from_pixel(6, 6, (((row+1)*320*6 + (col+1)*6)), BLACK);
                    board[row][col+1] = 'A';
                    screen_block_from_pixel(6, 6, (((row+1)*320*6 + (col+2)*6)), WHITE);
                }
            }
        }
    }
}

//Moves all the aliens in the board one position down in the board and on the screen.
void board_alien_move_down(char** board, int board_width, int board_hight){
    
    int row;
    int col;
    for (row = board_hight -1; row >= 0; row--) {
        for (col = board_width -1; col >= 0; col--) {
            //If there is an alien in a index in the table
            if (board[row][col] == 'A') {
                board[row+1][col] = 'A';
                board[row][col] = ' ';
                screen_block_from_pixel(6, 6, (((row+1)*320*6 + (col+1)*6)), BLACK);
                screen_block_from_pixel(6, 6, (((row+2)*320*6 + (col+1)*6)), WHITE);
            }
        }
    }
}

//Moves all the aliens in the board one position to the left in the board and on the screen.
void board_alien_move_left(char** board, int board_width, int board_hight){
    
    int row;
    int col;
    for (row = board_hight -1; row >= 0; row--) {
        for (col = 0; col < board_width; col++) {
            //If there is an alien in a index in the table
            if (board[row][col] == 'A') {
                if (board[row][col-1] == 'S'){
                    board_alien_hit(board, board_width, board_hight, row, col);
                    
                }else{
                    board[row][col-1] = 'A';
                    board[row][col] = ' ';
                    screen_block_from_pixel(6, 6, (((row+1)*320*6 + (col+1)*6)), BLACK);
                    screen_block_from_pixel(6, 6, (((row+1)*320*6 + (col)*6)), WHITE);
                }
            }
        }
    }
}

//Returns the row number of alien with the lowest row placement in the board
//Returns -1 if no alien can be found
int board_alien_find_lowest(char** board, int board_width, int board_hight){
    int row;
    int col;
    for (row = board_hight -1; row >= 0; row--) {
        for (col = 0; col < board_width; col++){
            if (board[row][col] == 'A'){
                return row;
            }
        }
    }
    return -1;
}


void board_player_move_right(char** board, int board_width, int board_hight){
    int row;
    int col;
    for (row = board_hight -1; row >= board_hight - 3; row--) {
        for (col = board_width-1; col >= 0; col--) {
            //If a player is found
            if (board[row][col] == 'P') {
                if (col == board_width -1) {
                    col = -1;
                }else {
                    board[row][col+1] = 'P';
                    board[row][col] = ' ';
                    screen_block_from_pixel(6, 6, (((row)*320*6 + (col+1)*6)), BLACK);
                    screen_block_from_pixel(6, 6, (((row)*320*6 + (col+2)*6)), GREEN);
                }
            }
        }
    }
}

//Moves the player one position to the left on the board and on the display.
void board_player_move_left(char** board, int board_width, int board_hight){
    int row;
    int col;
    for (row = board_hight -1; row >= board_hight - 3; row--) {
        for (col = 0; col < board_width; col++) {
            //If a player is found
            if (board[row][col] == 'P') {
                if (col == 0) {
                    col = board_width;
                }else{
                    board[row][col-1] = 'P';
                    board[row][col] = ' ';
                    screen_block_from_pixel(6, 6, (((row)*320*6 + (col+1)*6)), BLACK);
                    screen_block_from_pixel(6, 6, (((row)*320*6 + (col)*6)), GREEN);
                }
            }
        }
    }
}

//This function displays the player on the display.
//Usefull for game startup
void board_display_player(char** board, int board_width, int board_hight){
    int row;
    int col;
    //Search for the player
    for (row = board_hight -1; row >= board_hight - 3; row--) {
        for (col = 0; col < board_width; col++) {
            if (board[row][col] == 'P') {
                //Set the framebuffer
                screen_block_from_pixel(6, 6, (((row)*320*6 + (col+1)*6)), GREEN);
            }
        }
    }
}


//Moves the player.
//Movement depends on the direction and the players current position.
void board_player_move(char** board, int board_width, int board_hight, int player_direction){
    if (player_direction == 1) {
        board_player_move_right(board, board_width, board_hight);
    }
    if (player_direction == -1) {
        board_player_move_left(board, board_width, board_hight);
    }
}


//Adds a shot to the board and to the display.
//The shot is located abow the center of the player.
void board_player_shot(char** board, int board_width, int board_hight, int *board_player_shot_timer){
    if (*board_player_shot_timer > 50) {
        //Find the center of the player in the board
        int row = board_hight -2;
        int col;
        int center = 0;
        for (col = 0; col < board_width; col++) {
            if (board[row][col] == 'P') {
                center++;
                if (center == 2) {
                    center = col;
                    col = board_width;
                }
            }
        }
        //Create a player shoot abow the center of player
        board[row-2][center] = 'S';
        screen_block_from_pixel(6, 6, (((row-2)*320*6 + (col+1)*6)), RED);
        *board_player_shot_timer = 0;
    }
}

//Moves all the shots in the board and on the display one position upwords.
void board_shot_move(char** board, int board_width, int board_hight){
    int row;
    int col;
    for (row = 0; row < board_hight; row++) {
        for (col = 0; col < board_width; col++) {
            //If there is a shot
            if (board[row][col] == 'S') {
                //If its not row 0
                if (row != 0){
                    if (board[row-1][col] == 'A') {
                        board_alien_hit(board, board_width, board_hight, row-1, col-1);
                        board[row][col] = ' ';
                        screen_block_from_pixel(6, 6, (((row+1)*320*6 + (col+1)*6)), BLACK);
                    }
                    else {
                        board[row-1][col] = 'S';
                        board[row][col] = ' ';
                        screen_block_from_pixel(6, 6, (((row+1)*320*6 + (col+1)*6)), BLACK);
                        screen_block_from_pixel(6, 6, (((row)*320*6 + (col+1)*6)), RED);
                    }
                }
                else{
                    if (board[row][col] == 'S') {
                        board[row][col] = ' ';
                        screen_block_from_pixel(6, 6, ((320*6 + (col+1)*6)), BLACK);
                    }
                }
                
            }
        }
    }
}


//This function makes an explotion of the alien and its sorunding squares.
void board_alien_hit(char** board, int board_width, int board_hight, int alien_row, int alien_col){
    
    int row = alien_row;
    int col = alien_col;
    
    if (alien_row == 0) {row = 1;}
    if (alien_col == 0) {col = 1;}
    if (alien_row == board_hight) {row = board_hight - 1;}
    if (alien_col == board_width) {col = board_width - 1;}
    
    if (board[row-1][col-1] != ' ') {
        board[row-1][col-1] = 'X';
        screen_block_from_pixel(6, 6, (((row)*320*6 + (col)*6)), YELLOW);
    }
    if (board[row-1][col] != ' ') {
        board[row-1][col] = 'X';
        screen_block_from_pixel(6, 6, (((row)*320*6 + (col+1)*6)), YELLOW);
    }
    if (board[row-1][col+1] != ' ') {
        board[row-1][col+1] = 'X';
        screen_block_from_pixel(6, 6, (((row)*320*6 + (col+2)*6)), YELLOW);
    }
    if (board[row][col-1] != ' ') {
        board[row][col-1] = 'X';
        screen_block_from_pixel(6, 6, (((row+1)*320*6 + (col)*6)), YELLOW);
    }
    if (board[row][col] != ' ') {
        board[row][col] = 'X';
        screen_block_from_pixel(6, 6, (((row+1)*320*6 + (col+1)*6)), YELLOW);
    }
    if (board[row][col+1] != ' ') {
        board[row][col+1] = 'X';
        screen_block_from_pixel(6, 6, (((row+1)*320*6 + (col+2)*6)), YELLOW);
    }
    
    //Move into bullet
    if (board[row+1][col-1] != ' ') {
        board[row+1][col-1] = 'X';
        screen_block_from_pixel(6, 6, (((row+2)*320*6 + (col)*6)), YELLOW);
    }
    if (board[row+1][col] != ' ') {
        board[row+1][col] = 'X';
        screen_block_from_pixel(6, 6, (((row+2)*320*6 + (col+1)*6)), YELLOW);
    }
    if (board[row+1][col+1] != ' ') {
        board[row+1][col+1] = 'X';
        screen_block_from_pixel(6, 6, (((row+2)*320*6 + (col+2)*6)), YELLOW);
    }
}


//This function removes all explotions from the board
void board_explotion_remove(char** board, int board_width, int board_hight){
    int row;
    int col;
    for (row = 0; row < board_hight; row++) {
        for (col = 0; col < board_width; col++) {
            if (board[row][col] == 'X') {
                board[row][col] = ' ';
                screen_block_from_pixel(6, 6, (((row+1)*320*6 + (col+1)*6)), BLACK);
            }
        }
    }
}

//Returns 1 if there is more aliens on the board and 0 if there is no aliens on the board.
int board_alien_remaining(char** board, int board_width, int board_hight){
    int row;
    int col;
    for (row = 0; row < board_hight; row++) {
        for (col = 0; col < board_width; col++) {
            if (board[row][col] == 'A') {
                return 1;
            }
        }
    }
    return 0;
}












