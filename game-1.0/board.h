#ifndef BOARD_H_
#define BOARD_H_

//All functions is commented in the board.c file

char** board_create(int board_width, int board_height);
void board_init(char** board, int board_width, int board_height);
void board_print(char** board, int board_width, int board_height);
void board_delete(char** board, int board_width, int board_height);
int board_find_most_right_alien(char** board, int board_width, int board_height);
int board_find_most_left_alien(char** board, int board_width, int board_height);
void board_delete(char** board,int board_width, int board_hight);
void board_alien_move(char** board, int board_width, int board_hight, int* alien_direction);
void board_alien_move_right(char** board, int board_width, int board_hight);
void board_alien_move_left(char** board, int board_width, int board_hight);
void board_alien_move_down(char** board, int board_width, int board_hight);
int board_alien_find_lowest(char** board, int board_width, int board_hight);
void board_player_move_right(char** board, int board_width, int board_hight);
void board_player_move_left(char** board, int board_width, int board_hight);
void board_player_move(char** board, int board_width, int board_hight, int player_direction);
void board_player_shot(char** board, int board_width, int board_hight, int *board_player_shot_timer);
void board_shot_move(char** board, int board_width, int board_hight);
void board_alien_hit(char** board, int board_width, int board_hight, int alien_row, int alien_col);
void board_explotion_remove(char** board, int board_width, int board_hight);
int board_alien_remaining(char** board, int board_width, int board_hight);
void board_update_display();
void board_display_player(char** board, int board_width, int board_hight);

#endif
