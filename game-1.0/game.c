#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h> /* mmap */
#include <linux/fb.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <stdbool.h>
//#include "screen.h"
#include "board.c"

//Define button values as defined in register GPIO_IF
#define BUTTON_1 254
#define BUTTON_2 253
#define BUTTON_3 251
#define BUTTON_4 247
#define BUTTON_5 239
#define BUTTON_6 223
#define BUTTON_7 191
#define BUTTON_8 127

//Game board array height, width
#define BOARD_HIGHT 40
#define BOARD_WIDTH 40

void button_switch(FILE* file);
void gamepad_init();
uint16_t* gamepad_addr;
void GPIO_signal_handler(int signal);
char** board;
FILE* gamepad_file;

int main(int argc, char *argv[])
{
	printf("game: Start\n");
	
	//Initialize screen
	screen_init();

	//Initialize gamepad driver
	gamepad_init();

    //The main game loop
    while (1) {
    	//Create game board
		board = board_create(BOARD_HIGHT, BOARD_WIDTH);
		board_init(board, BOARD_HIGHT, BOARD_WIDTH);

		int run = 1;
	    int *alien_direction = malloc(sizeof(int));
	    *alien_direction = 1;
	    
	    int *board_player_shot_timer = malloc(sizeof(int));
	    *board_player_shot_timer = 1;
	    
	    int alien = 0;
	    int explotion = 0;
	    int player = 0;
	    int shot = 0;
	    board_display_player(board, BOARD_WIDTH, BOARD_HIGHT);
	    while (run) {
	        if (alien == 3){
	            board_alien_move(board, BOARD_WIDTH, BOARD_HIGHT, alien_direction);
	            alien = 0;
	        }
	        if (explotion == 2){
	            board_explotion_remove(board, BOARD_HIGHT, BOARD_WIDTH);
	            explotion = 0;
	        }
	        if (player == 1){
	            player = 0;
	        }
	        if (shot == 1){
	            board_shot_move(board, BOARD_WIDTH, BOARD_HIGHT);
	            shot = 0;
	        }
	        
	        if (board_alien_find_lowest(board, BOARD_WIDTH, BOARD_HIGHT) == 38){
	            screen_lose();
	            screen_update();
	            sleep(10);
	            run = 0;
	        }
	        if (!board_alien_remaining(board, BOARD_WIDTH, BOARD_HIGHT)){
	        	screen_print_win();
	        	sleep(10);
	            run = 0;
	        }
	        
	        alien++;
	        explotion++;
	        player++;
	        shot++;
	        board_update_display();
	        usleep(2000);
	    }

    //Cleanup game logic
    board_delete(board,BOARD_WIDTH, BOARD_HIGHT);
    free(alien_direction);
    free(board_player_shot_timer);

	}

	printf("game: End of program\n");
	exit(EXIT_SUCCESS);
}

//Initialize gamepad and activate interrupts
void gamepad_init(){

	//Open the gamepad driver file
	gamepad_file = fopen("/dev/gamepaddriver", "rb");

	//Enable asynchronous notification -----
	//Set handler to SIGIO signal
	struct sigaction act;
	memset (&act, 0, sizeof(act));
	act.sa_handler = &GPIO_signal_handler;
 	
	// The SA_SIGINFO flag tells sigaction() to use the sa_sigaction field, not sa_handler.
	act.sa_flags = 0;
	if (sigaction(SIGIO, &act, NULL) < 0) {
		printf("sigaction error\n");
	}

	//Set process to SIGIO signal
	if (fcntl(fileno(gamepad_file), F_SETOWN, getpid()) < 0){
		printf("Error when setting process to SIGIO signal\n");
	}
	//Get file access mode and file status flags
	int oflags;
	if (oflags = fcntl(fileno(gamepad_file), F_GETFL) < 0){
		printf("Error when getting file access mode and file status flags\n");
	} 
	//Set file status flags
	if (fcntl(fileno(gamepad_file), F_SETFL, oflags | FASYNC) < 0){
		printf("Error when setting file status flags \n");
	}// -----

	//Close file
	close(gamepad_file);
}
//Handle GPIO signal
void GPIO_signal_handler(int signo){
	if(signo == SIGIO){
		button_switch(gamepad_file);
	}
}
//Handle button interrupts
void button_switch(FILE* file){
	switch(getc(file)){
		case BUTTON_1: //Move player left
			board_player_move(board, BOARD_HIGHT, BOARD_WIDTH, -1);
			break;
		case BUTTON_2:
			break;
		case BUTTON_3: //Move player right
			board_player_move(board, BOARD_HIGHT, BOARD_WIDTH, 1);
			break;
		case BUTTON_4:
			break;
		case BUTTON_5:
			break;
		case BUTTON_6:
			break;
		case BUTTON_7: //Shoot
			board_player_shot(board, BOARD_HIGHT, BOARD_WIDTH, 1);
			break;
		case BUTTON_8:
			break;
		default:
			break;
	}
}