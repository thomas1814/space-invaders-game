#include "screen.h"
#include <unistd.h>

#define RED 	(int*)0b1111100000000000//31744
#define GREEN 	(int*)0b0000011111000000//240
#define BLUE 	(int*)0b0000000000111110
#define BLACK 	(int*)0b0000000000000000
#define WHITE 	(int*)0b1111111111111110
#define YELLOW 	(int*)0b1111111111000000
#define SCREEN_HEIGHT 240
#define SCREEN_WIDTH 320
#define SCREEN_SIZE (320*240)
#define SCREEN_BYTE_DEPTH 2
uint16_t* screen_addr;

//Display "Lose" on the display
void screen_lose(){
    
    //L
    screen_block_from_pixel(6, 6, (((5+1)*320*6 + (5+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((6+1)*320*6 + (5+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((7+1)*320*6 + (5+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((8+1)*320*6 + (5+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((9+1)*320*6 + (5+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((10+1)*320*6 + (5+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((11+1)*320*6 + (5+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((11+1)*320*6 + (6+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((11+1)*320*6 + (7+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((11+1)*320*6 + (8+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((11+1)*320*6 + (9+1)*6)), RED);
    
    //O
    screen_block_from_pixel(6, 6, (((5+1)*320*6 + (11+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((6+1)*320*6 + (11+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((7+1)*320*6 + (11+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((8+1)*320*6 + (11+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((9+1)*320*6 + (11+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((10+1)*320*6 + (11+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((11+1)*320*6 + (11+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((11+1)*320*6 + (12+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((11+1)*320*6 + (13+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((11+1)*320*6 + (14+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((11+1)*320*6 + (15+1)*6)), RED);
    
    screen_block_from_pixel(6, 6, (((5+1)*320*6 + (15+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((6+1)*320*6 + (15+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((7+1)*320*6 + (15+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((8+1)*320*6 + (15+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((9+1)*320*6 + (15+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((10+1)*320*6 + (15+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((5+1)*320*6 + (15+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((5+1)*320*6 + (12+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((5+1)*320*6 + (13+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((5+1)*320*6 + (14+1)*6)), RED);
    
    //S
    screen_block_from_pixel(6, 6, (((5+1)*320*6 + (17+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((5+1)*320*6 + (18+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((5+1)*320*6 + (19+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((5+1)*320*6 + (20+1)*6)), RED);
    
    screen_block_from_pixel(6, 6, (((6+1)*320*6 + (17+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((7+1)*320*6 + (17+1)*6)), RED);
    
    screen_block_from_pixel(6, 6, (((8+1)*320*6 + (17+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((8+1)*320*6 + (18+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((8+1)*320*6 + (19+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((8+1)*320*6 + (20+1)*6)), RED);
    
    screen_block_from_pixel(6, 6, (((9+1)*320*6 + (20+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((10+1)*320*6 + (20+1)*6)), RED);
    
    screen_block_from_pixel(6, 6, (((11+1)*320*6 + (17+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((11+1)*320*6 + (18+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((11+1)*320*6 + (19+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((11+1)*320*6 + (20+1)*6)), RED);
    
    
    //E
    screen_block_from_pixel(6, 6, (((5+1)*320*6 + (22+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((6+1)*320*6 + (22+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((7+1)*320*6 + (22+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((8+1)*320*6 + (22+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((9+1)*320*6 + (22+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((10+1)*320*6 + (22+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((11+1)*320*6 + (22+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((11+1)*320*6 + (23+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((11+1)*320*6 + (24+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((11+1)*320*6 + (25+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((11+1)*320*6 + (26+1)*6)), RED);
    
    screen_block_from_pixel(6, 6, (((5+1)*320*6 + (23+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((5+1)*320*6 + (24+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((5+1)*320*6 + (25+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((5+1)*320*6 + (26+1)*6)), RED);
    
    screen_block_from_pixel(6, 6, (((8+1)*320*6 + (23+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((8+1)*320*6 + (24+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((8+1)*320*6 + (25+1)*6)), RED);
    screen_block_from_pixel(6, 6, (((8+1)*320*6 + (26+1)*6)), RED);
    
}

void screen_init(){
	//Open framebuffer device
	FILE* screen_file;
	if ((screen_file = open("/dev/fb0", O_RDWR)) < 0) {
		printf("screen_init: error: Unable to open '/dev/fb0'. Terminating program...\n");
		exit(EXIT_FAILURE);
	}

	//Memory map file
	*screen_addr = (uint16_t*)malloc(sizeof(uint16_t*));
	screen_addr = (uint16_t*)malloc(SCREEN_SIZE*SCREEN_BYTE_DEPTH);
	screen_addr = mmap(NULL, SCREEN_SIZE*SCREEN_BYTE_DEPTH, PROT_READ | PROT_WRITE, MAP_SHARED, screen_file, 0);

	close(screen_file);
}

void screen_update(){
	screen_update_area(0,0,SCREEN_WIDTH,SCREEN_HEIGHT);
}

void screen_update_area(int pos_x, int pos_y, int width, int height){

	FILE* screen_file;
	if ((screen_file = open("/dev/fb0", O_RDWR)) < 0) {
		printf("screen_init: error: Unable to open '/dev/fb0'. Terminating program...\n");
		exit(EXIT_FAILURE);
	}

	//Set area to be updated
	struct fb_copyarea rect;
	rect.dx = pos_x;
	rect.dy = pos_y;
	rect.width = width;
	rect.height = height;

	//Tell driver to update display
	ioctl(screen_file, 0x4680, &rect);
	close(screen_file);
}

/* Draw a grid defined by 2D-array */
void screen_draw_board(char** board, int board_width, int board_height){
	//Blackout screen
	screen_black();


    int i = 0;
    int px_row = 0;
    int px_col = 0;
    int row = 0;
    int col = 0;
    int cell_size = 6;
    for (i=0;i<SCREEN_SIZE;i++){
    	/*if((px_row % cell_size) == 0){
			if (col < board_width){
				screen_set_pixel(i, BLUE);
			}
        }*/
    	if((i%SCREEN_WIDTH)==0){// && i != 0){
            px_row++;
            if((px_row % cell_size) == 0){
				row ++;
				col = 0;
			}
            px_col = 0;
	    }
	    if((px_row % cell_size) == 0) {
        	if((px_col % cell_size) == 0 && col < board_height){
	        	//printf("board: px: %d , (%d %d)\n", i, px_row, px_col);
	        	//printf("board: px: %d , (%d %d)\n", i, row, col);
		        switch (board[row][col]){
					case 'A':
						screen_block_from_pixel(cell_size, cell_size, i, WHITE);
						//printf("found A: %d, %d\n", row, col);
						break;
					case 'P':
						screen_block_from_pixel(cell_size, cell_size, i, GREEN);
						//printf("found P %d, %d\n", row, col);
						break;
					case 'S':
						screen_block_from_pixel(cell_size, cell_size, i, RED);
						//printf("found S %d, %d\n", row, col);
						break;
					case 'X':
						screen_block_from_pixel(cell_size, cell_size, i, YELLOW);
						//board_print(board, 40, 42);
						//printf("found X %d, %d\n", row, col);
						break;
					default:
						//nothing here
						break;
				}
			}
        }
	    if((px_col % cell_size) == 0){
			if (col == board_width+2){
				screen_set_pixel(i, RED);
			}
        }
		px_col++;
		if ((px_col%cell_size)==0){
				col++;
		}
    }
}

//Set pixel to color
void screen_set_pixel(int pxl, int color){
    screen_addr[pxl] = color;
}


/* Draw a block with length and height down right from pixel*/
void screen_block_from_pixel(int length, int height, int pxl, int color) {
	int i;
	int j;
	for (j = 0; j < (height); j++){
		for (i = 0; i < (length); i++){
			screen_set_pixel((pxl+i), color);
		}
		pxl += SCREEN_WIDTH;
	}
}

//Prints blacks to the screen
void screen_black(){
    int i = 0;
    while(i < SCREEN_SIZE){
    	screen_addr[i] = BLACK;
        i++;
    }
}

//Prints a hardcoded set of boxes to display "Win!"
void screen_print_win() {
	//Blackout screen
	screen_black();
	screen_block_from_pixel(6, 6, (((5)*320*6 + (5)*6)), RED);
	screen_block_from_pixel(6, 6, (((6)*320*6 + (5)*6)), RED);
	screen_block_from_pixel(6, 6, (((7)*320*6 + (5)*6)), RED);
	screen_block_from_pixel(6, 6, (((8)*320*6 + (5)*6)), RED);

	screen_block_from_pixel(6, 6, (((9)*320*6 + (6)*6)), RED);
	
	screen_block_from_pixel(6, 6, (((8)*320*6 + (7)*6)), RED);
	screen_block_from_pixel(6, 6, (((7)*320*6 + (7)*6)), RED);
	screen_block_from_pixel(6, 6, (((6)*320*6 + (7)*6)), RED);
	screen_block_from_pixel(6, 6, (((5)*320*6 + (7)*6)), RED);

	screen_block_from_pixel(6, 6, (((9)*320*6 + (8)*6)), RED);

	screen_block_from_pixel(6, 6, (((8)*320*6 + (9)*6)), RED);
	screen_block_from_pixel(6, 6, (((7)*320*6 + (9)*6)), RED);
	screen_block_from_pixel(6, 6, (((6)*320*6 + (9)*6)), RED);
	screen_block_from_pixel(6, 6, (((5)*320*6 + (9)*6)), RED);


	screen_block_from_pixel(6, 6, (((9)*320*6 + (11)*6)), RED);
	screen_block_from_pixel(6, 6, (((8)*320*6 + (11)*6)), RED);
	screen_block_from_pixel(6, 6, (((7)*320*6 + (11)*6)), RED);

	screen_block_from_pixel(6, 6, (((5)*320*6 + (11)*6)), RED);


	screen_block_from_pixel(6, 6, (((9)*320*6 + (13)*6)), RED);
	screen_block_from_pixel(6, 6, (((8)*320*6 + (13)*6)), RED);
	screen_block_from_pixel(6, 6, (((7)*320*6 + (13)*6)), RED);
	screen_block_from_pixel(6, 6, (((6)*320*6 + (13)*6)), RED);
	screen_block_from_pixel(6, 6, (((5)*320*6 + (13)*6)), RED);

	screen_block_from_pixel(6, 6, (((5)*320*6 + (14)*6)), RED);
	screen_block_from_pixel(6, 6, (((5)*320*6 + (15)*6)), RED);
	screen_block_from_pixel(6, 6, (((5)*320*6 + (16)*6)), RED);
	screen_block_from_pixel(6, 6, (((5)*320*6 + (17)*6)), RED);

	screen_block_from_pixel(6, 6, (((6)*320*6 + (17)*6)), RED);
	screen_block_from_pixel(6, 6, (((7)*320*6 + (17)*6)), RED);
	screen_block_from_pixel(6, 6, (((8)*320*6 + (17)*6)), RED);
	screen_block_from_pixel(6, 6, (((9)*320*6 + (17)*6)), RED);

	screen_block_from_pixel(6, 6, (((5)*320*6 + (19)*6)), RED);
	screen_block_from_pixel(6, 6, (((6)*320*6 + (19)*6)), RED);
	screen_block_from_pixel(6, 6, (((7)*320*6 + (19)*6)), RED);

	screen_block_from_pixel(6, 6, (((9)*320*6 + (19)*6)), RED);

	screen_update();
}