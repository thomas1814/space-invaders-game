#ifndef SCREEN_H_
#define SCREEN_H_

//All functions are commented in the screen.c file

void screen_init();
void screen_update();
void screen_update_area(int pos_x, int pos_y, int width, int height);
void screen_draw_board(char** board, int board_width, int board_height);
void screen_set_pixel(int pxl, int color);
void screen_block_from_pixel(int length, int height, int pxl, int color);
void screen_black();
void screen_white();
void screen_draw_board2(char** board, int board_width, int board_height);
void screen_lose();


#endif